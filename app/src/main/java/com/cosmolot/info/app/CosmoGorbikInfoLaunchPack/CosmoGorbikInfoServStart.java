package com.cosmolot.info.app.CosmoGorbikInfoLaunchPack;

import android.app.Application;

import com.onesignal.OneSignal;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

public class CosmoGorbikInfoServStart extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        cosmoGorbikInfoServices();
    }
    private void cosmoGorbikInfoServices(){
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        OneSignal.initWithContext(getApplicationContext());
        OneSignal.setAppId("7a4f0be5-1420-4502-a42d-7e53219e0670");

        YandexMetricaConfig cosmoGorbikInfoYandex = YandexMetricaConfig.newConfigBuilder("34cd7b41-8a3c-485b-b483-2f5766ed713b").build();
        YandexMetrica.activate(getApplicationContext(), cosmoGorbikInfoYandex);
        YandexMetrica.enableActivityAutoTracking(this);
    }
}
