package com.cosmolot.info.app.CosmoGorbikInfoViewPack;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.cosmolot.info.app.CosmoGorbikInfoLaunchPack.CosmoGorbikInfoConnection;
import com.cosmolot.info.app.CosmoGorbikInfoLaunchPack.CosmoGorbikInfoKeys;
import com.cosmolot.info.app.R;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.Context.MODE_PRIVATE;


public class CosmoGorbikInfoViewFrag extends Fragment {

    private Uri cosmoGorbikInfoUri;
    private ValueCallback<Uri[]> cosmoGorbikInfoCallBVal;
    private SwipeRefreshLayout cosmo_gorbik_info_swipe;
    private RelativeLayout cosmo_gorbik_info_relative_conn;
    private Boolean cosmoGorbikInfoIsReady;
    private SharedPreferences cosmoGorbikInfoPrefs;
    private String cosmo_gorbik_info_int_path;
    private WebView cosmo_gorbik_info_web;
    private IntentFilter cosmoGorbikInfoIntentFilter;
    private View cosmoGorbikInfoViewFragView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        cosmoGorbikInfoViewFragView=inflater.inflate(R.layout.fragment_cosmo_gorbik_info_view, container, false);
        cosmoGorbikInfoSetFields();
        cosmoGorbikInfoIsReady = false;
        cosmoGorbikInfoIntentFilter =new IntentFilter();
        cosmoGorbikInfoIntentFilter.addAction(CosmoGorbikInfoKeys.COSMO_GORBIK_INFO_CONNECT);
        Intent cosmoGorbikInfoIntentRec=new Intent(getContext(), CosmoGorbikInfoConnection.class);
        requireActivity().startService(cosmoGorbikInfoIntentRec);
        requireActivity().getOnBackPressedDispatcher().addCallback(requireActivity(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (cosmo_gorbik_info_web != null && cosmo_gorbik_info_web.canGoBack()) {
                    cosmo_gorbik_info_web.goBack();
                } else if (cosmo_gorbik_info_web != null && !cosmo_gorbik_info_web.canGoBack() && !cosmo_gorbik_info_web.getUrl().equals(cosmo_gorbik_info_int_path)) {
                    cosmo_gorbik_info_web.loadUrl(cosmo_gorbik_info_int_path);
                } else {
                    requireActivity().finish();
                }
            }
        });
        cosmoGorbikInfoUri = Uri.EMPTY;
        cosmoGorbikInfoSetCookies();
        cosmoGorbikInfoWebSet();
        cosmo_gorbik_info_swipe.setOnRefreshListener(()->{
            startCosmoGorbikInfo();
            cosmo_gorbik_info_swipe.setRefreshing(false);
        });

        return cosmoGorbikInfoViewFragView;
    }
    private void cosmoGorbikInfoSetCookies(){
        CookieManager cosmoGorbikInfoCookies = CookieManager.getInstance();
        CookieManager.setAcceptFileSchemeCookies(true);
        cosmoGorbikInfoCookies.setAcceptThirdPartyCookies(cosmo_gorbik_info_web, true);
    }
    @SuppressLint("SetJavaScriptEnabled")
    public void cosmoGorbikInfoWebSet() {
        cosmo_gorbik_info_web.getSettings().setDisplayZoomControls(false);
        cosmo_gorbik_info_web.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        cosmo_gorbik_info_web.getSettings().setUseWideViewPort(true);
        cosmo_gorbik_info_web.getSettings().setBuiltInZoomControls(true);
        cosmo_gorbik_info_web.getSettings().setJavaScriptEnabled(true);
        cosmo_gorbik_info_web.getSettings().setDefaultTextEncodingName("utf-8");
        cosmo_gorbik_info_web.getSettings().setDomStorageEnabled(true);
        cosmo_gorbik_info_web.getSettings().setLoadWithOverviewMode(true);
        cosmo_gorbik_info_web.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        cosmo_gorbik_info_web.setWebChromeClient(new WebChromeClient(){

            @SuppressLint("QueryPermissionsNeeded")
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                cosmoGorbikInfoPermissionsCheck();
                cosmoGorbikInfoCallBVal = filePathCallback;
                Intent cosmoGorbikInfoIntentPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (cosmoGorbikInfoIntentPhoto.resolveActivity(requireActivity().getPackageManager()) != null) {
                    File cosmoGorbikInfoPhotoFile = null;
                    try {
                        cosmoGorbikInfoPhotoFile = cosmoGorbikInfoPictureCreate();
                    } catch (IOException ex){
                        Log.e("Cosmo Gorbik Info", "Photo Error !", ex);
                    }

                    if (cosmoGorbikInfoPhotoFile != null) {
                        Uri cosmoGorbikInfoUriPic = FileProvider.getUriForFile(
                                requireContext(),
                                requireActivity().getApplication().getPackageName() +".provider",
                                cosmoGorbikInfoPhotoFile
                        );
                        cosmoGorbikInfoUri = cosmoGorbikInfoUriPic;
                        cosmoGorbikInfoIntentPhoto.putExtra(MediaStore.EXTRA_OUTPUT, cosmoGorbikInfoUriPic);
                        cosmoGorbikInfoIntentPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        Intent cosmoGorbikInfoGetIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        cosmoGorbikInfoGetIntent.addCategory(Intent.CATEGORY_OPENABLE);
                        cosmoGorbikInfoGetIntent.setType("image/*");
                        Intent[] cosmoGorbikInfoIntentArr = {cosmoGorbikInfoGetIntent};
                        Intent cosmoGorbikInfoChooserInt = new Intent(Intent.ACTION_CHOOSER);
                        cosmoGorbikInfoChooserInt.putExtra(Intent.EXTRA_INTENT, cosmoGorbikInfoIntentPhoto);
                        cosmoGorbikInfoChooserInt.putExtra(Intent.EXTRA_INITIAL_INTENTS, cosmoGorbikInfoIntentArr);
                        startActivityForResult(cosmoGorbikInfoChooserInt, CosmoGorbikInfoKeys.COSMO_GORBIK_INFO_PICKER);
                        return true;
                    }
                }
                return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
            }
        });
        cosmo_gorbik_info_web.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("mailto:")||url.startsWith("tel:")) {
                    Intent cosmoGorbikInfoViewIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(cosmoGorbikInfoViewIntent);
                }
                else if (url.contains("twitter")||url.contains("facebook")||url.contains("instagram")||url.contains("linkedin")||url.contains("vk.com")){
                    Log.d("Cosmo Gorbik Info","Socials Stop");
                }
                else {view.loadUrl(url);}
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                cosmoGorbikLastInfoSave(url);
            }
        });

        cosmoGorbikInfoLoadInternetPath();
        startCosmoGorbikInfo();
    }
    private void cosmoGorbikInfoSetFields(){
        cosmo_gorbik_info_swipe = cosmoGorbikInfoViewFragView.findViewById(R.id.cosmo_gorbik_info_swipe);
        cosmo_gorbik_info_relative_conn = cosmoGorbikInfoViewFragView.findViewById(R.id.cosmo_gorbik_info_relative_conn);
        cosmo_gorbik_info_web = cosmoGorbikInfoViewFragView.findViewById(R.id.cosmo_gorbik_info_web);

    }
    private void cosmoGorbikInfoPermissionsCheck() {
        Dexter.withContext(getContext()).withPermissions(CAMERA,WRITE_EXTERNAL_STORAGE)
                .withListener( new MultiplePermissionsListener(){
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {}
                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {}
                }).check();
    }
    private File cosmoGorbikInfoPictureCreate() throws IOException {
        String cosmoGorbikInfoTimeStampp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String cosmoGorbikInfoNameOfPic = "COSMO_GORBIK_INFO_ONE" + cosmoGorbikInfoTimeStampp + "_";
        File cosmoGorbikInfoFilePath = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(cosmoGorbikInfoNameOfPic, ".jpg", cosmoGorbikInfoFilePath);
    }

    private void cosmoGorbikLastInfoSave(String cosmoGorbikLastInfo) {
        cosmoGorbikInfoPrefs = requireActivity().getSharedPreferences("cosmoGorbikInfoPrefs", MODE_PRIVATE);
        SharedPreferences.Editor cosmoGorbikInfoEditor = cosmoGorbikInfoPrefs.edit();
        cosmoGorbikInfoEditor.putString("cosmoGorbikLastInfo", cosmoGorbikLastInfo);
        cosmoGorbikInfoEditor.apply();
    }
    public void cosmoGorbikInfoShow(){
        cosmo_gorbik_info_swipe.setVisibility(View.VISIBLE);
        cosmo_gorbik_info_relative_conn.setVisibility(View.INVISIBLE);
        cosmoGorbikInfoIsReady = true;
    }
    public void cosmoGorbikInfoHide(){
        cosmo_gorbik_info_swipe.setVisibility(View.INVISIBLE);
        cosmo_gorbik_info_relative_conn.setVisibility(View.VISIBLE);
        cosmoGorbikInfoIsReady = false;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CosmoGorbikInfoKeys.COSMO_GORBIK_INFO_PICKER) {
            if (cosmoGorbikInfoCallBVal == null) return;
            Uri cosmoGorbikInfoResUri = (data == null || resultCode != Activity.RESULT_OK) ? null : data.getData();
            if (cosmoGorbikInfoResUri != null && cosmoGorbikInfoCallBVal != null) {
                cosmoGorbikInfoCallBVal.onReceiveValue(new Uri[]{cosmoGorbikInfoResUri});
            } else if (cosmoGorbikInfoCallBVal != null) {
                cosmoGorbikInfoCallBVal.onReceiveValue(new Uri[]{cosmoGorbikInfoUri});
            }

        }
        cosmoGorbikInfoCallBVal = null;
        super.onActivityResult(requestCode, resultCode, data);
    }
    public void cosmoGorbikInfoLoadInternetPath() {
        cosmo_gorbik_info_int_path = requireActivity()
                .getSharedPreferences("cosmoGorbikInfoPrefs", MODE_PRIVATE)
                .getString("cosmo_gorbik_info_int_path", "");

    }
    private String cosmoGorbikLastInfoLoad() {
        return requireActivity().getSharedPreferences("cosmoGorbikInfoPrefs", MODE_PRIVATE).getString("cosmoGorbikLastInfo", "");
    }
    public BroadcastReceiver cosmoGorbikInfoReceiverB =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CosmoGorbikInfoKeys.COSMO_GORBIK_INFO_CONNECT)){
                if (intent.getStringExtra("COSMO_GORBIK_INFO_CONNECT").equals("true")){
                    if (!cosmoGorbikInfoIsReady){
                        cosmoGorbikInfoShow();
                    }
                }
                else { cosmoGorbikInfoHide(); }
            }
        }
    };
    @Override
    public void onPause() {
        super.onPause();
        requireActivity().unregisterReceiver(cosmoGorbikInfoReceiverB);
    }
    @Override
    public void onResume() {
        super.onResume();
        requireActivity().registerReceiver(cosmoGorbikInfoReceiverB, cosmoGorbikInfoIntentFilter);
    }
    public void startCosmoGorbikInfo() {
        if (cosmoGorbikLastInfoLoad().equals("")) {
            cosmo_gorbik_info_web.loadUrl(cosmo_gorbik_info_int_path);
        } else {
            cosmo_gorbik_info_web.loadUrl(cosmoGorbikLastInfoLoad());
        }
    }

}