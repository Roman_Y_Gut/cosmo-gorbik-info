package com.cosmolot.info.app.CosmoGorbikInfoLaunchPack;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

import androidx.annotation.Nullable;

public class CosmoGorbikInfoConnection extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Cosmo Gorbik Info Connected");
    }
    Handler cosmoGorbikInfoHandler =new Handler();
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        cosmoGorbikInfoHandler.post(cosmoGorbikInfoRun);
        return START_STICKY;
    }
    public boolean cosmoGorbikInfoIsCheckConn(Context cosmoGorbikInfoContext){
        ConnectivityManager cosmoGorbikInfoConnMan=(ConnectivityManager)cosmoGorbikInfoContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo cosmoGorbikInfoInfo=cosmoGorbikInfoConnMan.getActiveNetworkInfo();
        return cosmoGorbikInfoInfo != null && cosmoGorbikInfoInfo.isConnectedOrConnecting();
    }

    private final Runnable cosmoGorbikInfoRun = new Runnable() {
        @Override
        public void run() {
            cosmoGorbikInfoHandler.postDelayed(cosmoGorbikInfoRun,1000- SystemClock.elapsedRealtime()%440);
            Intent cosmoGorbikInfoIntentConn=new Intent();
            cosmoGorbikInfoIntentConn.setAction(CosmoGorbikInfoKeys.COSMO_GORBIK_INFO_CONNECT);
            cosmoGorbikInfoIntentConn.putExtra("COSMO_GORBIK_INFO_CONNECT",""+ cosmoGorbikInfoIsCheckConn(CosmoGorbikInfoConnection.this));
            sendBroadcast(cosmoGorbikInfoIntentConn);
        }
    };
}
