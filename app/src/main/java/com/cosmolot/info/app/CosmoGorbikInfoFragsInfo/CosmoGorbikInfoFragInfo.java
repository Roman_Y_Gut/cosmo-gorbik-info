package com.cosmolot.info.app.CosmoGorbikInfoFragsInfo;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cosmolot.info.app.R;
import com.skydoves.expandablelayout.ExpandableLayout;


public class CosmoGorbikInfoFragInfo extends Fragment {

    private View cosmoGorbikInfoFragInfoView;
    public ExpandableLayout cosmo_gorbik_info_first_info_expandable,
            cosmo_gorbik_info_second_info_expandable,cosmo_gorbik_info_third_info_expandable;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        cosmoGorbikInfoFragInfoView=inflater.inflate(R.layout.fragment_cosmo_gorbik_info_frag_info, container, false);

        cosmo_gorbik_info_first_info_expandable =cosmoGorbikInfoFragInfoView.findViewById(R.id.cosmo_gorbik_info_first_info_expandable);
        cosmo_gorbik_info_second_info_expandable =cosmoGorbikInfoFragInfoView.findViewById(R.id.cosmo_gorbik_info_second_info_expandable);
        cosmo_gorbik_info_third_info_expandable=cosmoGorbikInfoFragInfoView.findViewById(R.id.cosmo_gorbik_info_third_info_expandable);

        cosmo_gorbik_info_first_info_expandable.parentLayout.setOnClickListener(view ->
        {
            if (cosmo_gorbik_info_first_info_expandable.isExpanded())
                cosmo_gorbik_info_first_info_expandable.collapse();
            else cosmo_gorbik_info_first_info_expandable.expand();
        });

        cosmo_gorbik_info_second_info_expandable.parentLayout.setOnClickListener(view ->
        {
            if (cosmo_gorbik_info_second_info_expandable.isExpanded())
                cosmo_gorbik_info_second_info_expandable.collapse();
            else cosmo_gorbik_info_second_info_expandable.expand();
        });

        cosmo_gorbik_info_third_info_expandable.parentLayout.setOnClickListener(view ->
        {
            if (cosmo_gorbik_info_third_info_expandable.isExpanded())
                cosmo_gorbik_info_third_info_expandable.collapse();
            else cosmo_gorbik_info_third_info_expandable.expand();
        });
        cosmoGorbikInfoSetAllText();

        return cosmoGorbikInfoFragInfoView;
    }
    private void cosmoGorbikInfoSetAllText(){
        TextView cosmoGorbikInfoFirstParentText=cosmo_gorbik_info_first_info_expandable.
                parentLayout.findViewById(R.id.cosmo_gorbik_parent_info_text);
        cosmoGorbikInfoFirstParentText.setText(R.string.info_title_one);

        TextView cosmoGorbikInfoSecondParentText=cosmo_gorbik_info_second_info_expandable.
                parentLayout.findViewById(R.id.cosmo_gorbik_parent_info_text);
        cosmoGorbikInfoSecondParentText.setText(R.string.info_title_two);

        TextView cosmoGorbikInfoThirdParentText=cosmo_gorbik_info_third_info_expandable.
                parentLayout.findViewById(R.id.cosmo_gorbik_parent_info_text);
        cosmoGorbikInfoThirdParentText.setText(R.string.info_title_three);

        TextView cosmoGorbikInfoFirstChildText=cosmo_gorbik_info_first_info_expandable.secondLayout.
                findViewById(R.id.cosmo_gorbik_child_info_text);
        cosmoGorbikInfoFirstChildText.setText(R.string.info_text_one);

        TextView cosmoGorbikInfoSecondChildText=cosmo_gorbik_info_second_info_expandable.secondLayout.
                findViewById(R.id.cosmo_gorbik_child_info_text);
        cosmoGorbikInfoSecondChildText.setText(R.string.info_text_two);

        TextView cosmoGorbikInfoThirdChildText=cosmo_gorbik_info_third_info_expandable.secondLayout.
                findViewById(R.id.cosmo_gorbik_child_info_text);
        cosmoGorbikInfoThirdChildText.setText(R.string.info_text_three);

    }
}