package com.cosmolot.info.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.widget.ProgressBar;

import com.cosmolot.info.app.CosmoGorbikInfoFragsInfo.CosmoGorbikInfoActInfo;
import com.cosmolot.info.app.CosmoGorbikInfoViewPack.CosmoGorbikInfoViewAct;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

public class CosmoGorbikInfoMain extends AppCompatActivity {

    private ProgressBar cosmo_gorbik_info_progress_bar;
    private Handler cosmoGorbikInfoHandler =new Handler();
    private int cosmoGorbikInfoCounting = 0;
    private String cosmo_gorbik_info_int_path;
    private SharedPreferences cosmoGorbikInfoPrefs;
    private FirebaseRemoteConfig cosmoGorbikInfoFireB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cosmo_gorbik_info_main);
        cosmoGorbikInfoFirebaseGet();
        cosmoGorbikInfoProgress();
    }
    private void cosmoGorbikInfoProgress(){
        cosmo_gorbik_info_progress_bar =findViewById(R.id.cosmo_gorbik_info_progress_bar);
        new Thread(() -> {
            while (cosmoGorbikInfoCounting <100){
                cosmoGorbikInfoCounting++;
                SystemClock.sleep(53);
                cosmoGorbikInfoHandler.post(() -> cosmo_gorbik_info_progress_bar.setProgress(cosmoGorbikInfoCounting));
            }
            cosmoGorbikInfoHandler.post(() -> {
                runOnUiThread(() -> {
                    if (getSharedPreferences("cosmoGorbikInfoPrefs", MODE_PRIVATE)
                            .getString("cosmo_gorbik_info_int_path", "").equals("cosmo_gorbik_info_int_path")){
                        startActivity(new Intent(CosmoGorbikInfoMain.this, CosmoGorbikInfoActInfo.class));
                    }
                    else{
                        startActivity(new Intent(CosmoGorbikInfoMain.this, CosmoGorbikInfoViewAct.class));
                    }
                });
            });
        }).start();
    }
    public void cosmoGorbikInfoFirebaseGet() {
        cosmoGorbikInfoFireB = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings cosmoGorbikInfoFireRemote = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(2520)
                .build();
        cosmoGorbikInfoFireB.setDefaultsAsync(R.xml.cosmo_gorbik_info_int_path);
        cosmoGorbikInfoFireB.setConfigSettingsAsync(cosmoGorbikInfoFireRemote);
        cosmoGorbikInfoFireB.fetchAndActivate().addOnCompleteListener(task -> {
            cosmo_gorbik_info_int_path = cosmoGorbikInfoFireB.getString("cosmo_gorbik_info_int_path");
            cosmoGorbikInfoPrefs = getSharedPreferences("cosmoGorbikInfoPrefs", MODE_PRIVATE);
            SharedPreferences.Editor cosmoGorbikInfoEditor = cosmoGorbikInfoPrefs.edit();
            cosmoGorbikInfoEditor.putString("cosmo_gorbik_info_int_path", cosmo_gorbik_info_int_path);
            cosmoGorbikInfoEditor.apply();
        });
    }
}