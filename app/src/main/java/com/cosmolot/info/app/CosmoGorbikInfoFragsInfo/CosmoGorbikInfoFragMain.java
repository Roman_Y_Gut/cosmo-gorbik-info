package com.cosmolot.info.app.CosmoGorbikInfoFragsInfo;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cosmolot.info.app.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class CosmoGorbikInfoFragMain extends Fragment {


    private View cosmoGorbikInfoFragMainView;
    private ImageView cosmo_gorbik_info_live_btn,cosmo_gorbik_info_finished_btn,cosmo_gorbik_info_second_finished_btn;
    private Dialog cosmoGorbikInfoLiveDialog,cosmoGorbikInfoFinishedDialog,cosmoGorbikInfoSecondFinishedDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        cosmoGorbikInfoFragMainView=inflater.inflate(R.layout.fragment_cosmo_gorbik_info_frag_main, container, false);
        cosmo_gorbik_info_live_btn=cosmoGorbikInfoFragMainView.findViewById(R.id.cosmo_gorbik_info_live_btn);
        cosmo_gorbik_info_finished_btn=cosmoGorbikInfoFragMainView.findViewById(R.id.cosmo_gorbik_info_finished_btn);
        cosmo_gorbik_info_second_finished_btn=cosmoGorbikInfoFragMainView.findViewById(R.id.cosmo_gorbik_info_second_finished_btn);
        cosmoGorbikInfoLiveDialog =new Dialog(getActivity());
        cosmoGorbikInfoFinishedDialog =new Dialog(getActivity());
        cosmoGorbikInfoSecondFinishedDialog=new Dialog(getActivity());

        cosmo_gorbik_info_live_btn.setOnClickListener(view -> {
            cosmoGorbikInfoLiveDialog.setContentView(R.layout.cosmo_gorbik_info_live_popup);

            @SuppressLint("SimpleDateFormat") SimpleDateFormat cosmoGorbikInfoDateFormat=new SimpleDateFormat("dd.MM.yyyy");
            Calendar cosmoGorbikInfoCal = Calendar.getInstance();
            Calendar cosmoGorbikInfoCal1 = Calendar.getInstance();
            Calendar cosmoGorbikInfoCal2 = Calendar.getInstance();
            Calendar cosmoGorbikInfoCal3 = Calendar.getInstance();
            cosmoGorbikInfoCal1.add(Calendar.DATE,2);
            cosmoGorbikInfoCal2.add(Calendar.DATE,4);
            cosmoGorbikInfoCal3.add(Calendar.DATE,6);
            String cosmoGorbikInfoFirstDate= cosmoGorbikInfoDateFormat.format(cosmoGorbikInfoCal.getTime());
            String cosmoGorbikInfoSecondDate= cosmoGorbikInfoDateFormat.format(cosmoGorbikInfoCal1.getTime());
            String cosmoGorbikInfoThirdDate= cosmoGorbikInfoDateFormat.format(cosmoGorbikInfoCal2.getTime());
            String cosmoGorbikInfoFourthDate= cosmoGorbikInfoDateFormat.format(cosmoGorbikInfoCal3.getTime());

            TextView cosmo_gorbik_info_live_pogruz_first_date=cosmoGorbikInfoLiveDialog.findViewById(R.id.cosmo_gorbik_info_live_pogruz_first_date);
            cosmo_gorbik_info_live_pogruz_first_date.setText(cosmoGorbikInfoFirstDate);
            TextView cosmo_gorbik_info_live_pogruz_second_date=cosmoGorbikInfoLiveDialog.findViewById(R.id.cosmo_gorbik_info_live_pogruz_second_date);
            cosmo_gorbik_info_live_pogruz_second_date.setText(cosmoGorbikInfoSecondDate);

            TextView cosmo_gorbik_info_live_jin_first_date=cosmoGorbikInfoLiveDialog.findViewById(R.id.cosmo_gorbik_info_live_jin_first_date);
            cosmo_gorbik_info_live_jin_first_date.setText(cosmoGorbikInfoSecondDate);
            TextView cosmo_gorbik_info_live_jin_second_date=cosmoGorbikInfoLiveDialog.findViewById(R.id.cosmo_gorbik_info_live_jin_second_date);
            cosmo_gorbik_info_live_jin_second_date.setText(cosmoGorbikInfoThirdDate);

            TextView cosmo_gorbik_info_live_john_first_date=cosmoGorbikInfoLiveDialog.findViewById(R.id.cosmo_gorbik_info_live_john_first_date);
            cosmo_gorbik_info_live_john_first_date.setText(cosmoGorbikInfoThirdDate);
            TextView cosmo_gorbik_info_live_john_second_date=cosmoGorbikInfoLiveDialog.findViewById(R.id.cosmo_gorbik_info_live_john_second_date);
            cosmo_gorbik_info_live_john_second_date.setText(cosmoGorbikInfoFourthDate);

            ImageView cosmo_gorbik_info_live_close;
            cosmo_gorbik_info_live_close=cosmoGorbikInfoLiveDialog.findViewById(R.id.cosmo_gorbik_info_live_close);
            cosmo_gorbik_info_live_close.setOnClickListener(view1 -> cosmoGorbikInfoLiveDialog.dismiss());
            cosmoGorbikInfoLiveDialog.show();
        });

        cosmo_gorbik_info_finished_btn.setOnClickListener(view -> {
            cosmoGorbikInfoFinishedDialog.setContentView(R.layout.cosmo_gorbik_info_finished_popup);
            ImageView cosmo_gorbik_info_finished_close;
            cosmo_gorbik_info_finished_close=cosmoGorbikInfoFinishedDialog.findViewById(R.id.cosmo_gorbik_info_finished_close);
            cosmo_gorbik_info_finished_close.setOnClickListener(view1 -> cosmoGorbikInfoFinishedDialog.dismiss());
            cosmoGorbikInfoFinishedDialog.show();
        });

        cosmo_gorbik_info_second_finished_btn.setOnClickListener(view -> {
            cosmoGorbikInfoSecondFinishedDialog.setContentView(R.layout.cosmo_gorbik_info_finished_second_popup);
            ImageView cosmo_gorbik_info_finished_second_close;
            cosmo_gorbik_info_finished_second_close=cosmoGorbikInfoSecondFinishedDialog.findViewById(R.id.cosmo_gorbik_info_finished_second_close);
            cosmo_gorbik_info_finished_second_close.setOnClickListener(view1 -> cosmoGorbikInfoSecondFinishedDialog.dismiss());
            cosmoGorbikInfoSecondFinishedDialog.show();
        });

        return cosmoGorbikInfoFragMainView;
    }
}