package com.cosmolot.info.app.CosmoGorbikInfoViewPack;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.cosmolot.info.app.R;

public class CosmoGorbikInfoViewAct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cosmo_gorbik_info_view);
        startCosmoGorbikView();
    }
    private void startCosmoGorbikView() {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out)
                .replace(R.id.cosmo_gorbik_info_view_holder,new CosmoGorbikInfoViewFrag()).commit();
    }
}