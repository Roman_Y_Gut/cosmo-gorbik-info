package com.cosmolot.info.app.CosmoGorbikInfoFragsInfo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.cosmolot.info.app.R;

import java.util.List;

public class CosmoGorbikInfoActInfo extends AppCompatActivity {

    private ImageView cosmo_gorbik_info_info_image,cosmo_gorbik_info_help_image,cosmo_gorbik_info_money_img;
    private RelativeLayout cosmo_gorbik_info_money_inner_rel,cosmo_gorbik_info_money_rel;
    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cosmo_gorbik_info_act_info);
        setCosmoGorbikInfoFields();

        cosmo_gorbik_info_info_image.setOnClickListener(view -> {
            if (getCurrentCosmoGorbikInfoFragment() instanceof CosmoGorbikInfoFragInfo) {

            }else {
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.cosmo_gorbik_info_slide_in_left,R.anim.cosmo_gorbik_info_slide_out_left,
                                R.anim.cosmo_gorbik_info_slide_out_right,R.anim.cosmo_gorbik_info_slide_in_right)
                        .replace(R.id.cosmo_gorbik_info_info_holder,new CosmoGorbikInfoFragInfo()).commit();
                cosmo_gorbik_info_help_image.setImageResource(R.drawable.cosmo_gorbik_info_help);
                cosmo_gorbik_info_info_image.setImageResource(R.drawable.cosmo_gorbik_info_info_red);
                cosmo_gorbik_info_info_image.setPadding(34,34,34,34);
                cosmo_gorbik_info_money_img.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                        R.color.white));
                cosmo_gorbik_info_money_inner_rel.setBackgroundResource(R.drawable.cosmo_gorbik_info_inner_shape);
                cosmo_gorbik_info_money_rel.setBackgroundResource(R.drawable.cosmo_gorbik_info_outer_shape);
            }

        });
        cosmo_gorbik_info_help_image.setOnClickListener(view -> {
            if (getCurrentCosmoGorbikInfoFragment() instanceof CosmoGorbikInfoFragQuestion) {

            }else {
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.cosmo_gorbik_info_slide_in_left,R.anim.cosmo_gorbik_info_slide_out_left,
                                R.anim.cosmo_gorbik_info_slide_out_right,R.anim.cosmo_gorbik_info_slide_in_right)
                        .replace(R.id.cosmo_gorbik_info_info_holder, new CosmoGorbikInfoFragQuestion()).commit();
                cosmo_gorbik_info_help_image.setImageResource(R.drawable.cosmo_gorbik_info_help_red);
                cosmo_gorbik_info_info_image.setImageResource(R.drawable.cosmo_gorbik_info_info_white);
                cosmo_gorbik_info_info_image.setPadding(50,50,50,50);
                cosmo_gorbik_info_money_img.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                        R.color.white));
                cosmo_gorbik_info_money_inner_rel.setBackgroundResource(R.drawable.cosmo_gorbik_info_inner_shape);
                cosmo_gorbik_info_money_rel.setBackgroundResource(R.drawable.cosmo_gorbik_info_outer_shape);
            }
        });
        cosmo_gorbik_info_money_rel.setOnClickListener(view -> {
            if (getCurrentCosmoGorbikInfoFragment() instanceof CosmoGorbikInfoFragMain) {

            }else {
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.cosmo_gorbik_info_slide_in_left,R.anim.cosmo_gorbik_info_slide_out_left,
                                R.anim.cosmo_gorbik_info_slide_out_right,R.anim.cosmo_gorbik_info_slide_in_right)
                        .replace(R.id.cosmo_gorbik_info_info_holder, new CosmoGorbikInfoFragMain()).commit();
                cosmo_gorbik_info_money_img.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                        R.color.black));
                cosmo_gorbik_info_money_inner_rel.setBackgroundResource(R.drawable.cosmo_gorbik_info_rel_stroke_shape);
                cosmo_gorbik_info_money_rel.setBackgroundResource(R.drawable.cosmo_gorbik_info_elipse);
                cosmo_gorbik_info_info_image.setImageResource(R.drawable.cosmo_gorbik_info_info_white);
                cosmo_gorbik_info_info_image.setPadding(50,50,50,50);
                cosmo_gorbik_info_help_image.setImageResource(R.drawable.cosmo_gorbik_info_help);
            }
        });

        if (savedInstanceState==null){
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.cosmo_gorbik_info_slide_in_left,R.anim.cosmo_gorbik_info_slide_out_left,
                            R.anim.cosmo_gorbik_info_slide_out_right,R.anim.cosmo_gorbik_info_slide_in_right)
                    .replace(R.id.cosmo_gorbik_info_info_holder,new CosmoGorbikInfoFragMain()).commit();
            cosmo_gorbik_info_money_img.setImageResource(R.drawable.cosmo_gorbik_info_money_black);
            cosmo_gorbik_info_money_inner_rel.setBackgroundResource(R.drawable.cosmo_gorbik_info_rel_stroke_shape);
            cosmo_gorbik_info_money_rel.setBackgroundResource(R.drawable.cosmo_gorbik_info_elipse);
            cosmo_gorbik_info_info_image.setImageResource(R.drawable.cosmo_gorbik_info_info_white);
            cosmo_gorbik_info_info_image.setPadding(50,50,50,50);
            cosmo_gorbik_info_help_image.setImageResource(R.drawable.cosmo_gorbik_info_help);
        }

    }
    private void setCosmoGorbikInfoFields(){
        cosmo_gorbik_info_info_image=findViewById(R.id.cosmo_gorbik_info_info_image);
        cosmo_gorbik_info_help_image=findViewById(R.id.cosmo_gorbik_info_help_image);
        cosmo_gorbik_info_money_img=findViewById(R.id.cosmo_gorbik_info_money_img);
        cosmo_gorbik_info_money_inner_rel=findViewById(R.id.cosmo_gorbik_info_money_inner_rel);
        cosmo_gorbik_info_money_rel=findViewById(R.id.cosmo_gorbik_info_money_rel);
    }
    private Fragment getCurrentCosmoGorbikInfoFragment() {
        FragmentManager cosmoGorbikInfoFragMan = CosmoGorbikInfoActInfo.this.getSupportFragmentManager();
        List<Fragment> cosmoGorbikInfoFragments = cosmoGorbikInfoFragMan.getFragments();
        for (Fragment cosmoGorbikInfoFrag : cosmoGorbikInfoFragments) {
            if (cosmoGorbikInfoFrag != null && cosmoGorbikInfoFrag.isVisible())
                return cosmoGorbikInfoFrag;
        }
        return null;
    }
}