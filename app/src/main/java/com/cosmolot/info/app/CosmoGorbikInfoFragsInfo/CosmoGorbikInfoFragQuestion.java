package com.cosmolot.info.app.CosmoGorbikInfoFragsInfo;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cosmolot.info.app.R;
import com.skydoves.expandablelayout.ExpandableLayout;


public class CosmoGorbikInfoFragQuestion extends Fragment {

    private View cosmoGorbikInfoFragQuestionView;
    public ExpandableLayout cosmo_gorbik_info_first_question_expandable,cosmo_gorbik_info_second_question_expandable,
            cosmo_gorbik_info_third_question_expandable,cosmo_gorbik_info_fourth_question_expandable,
            cosmo_gorbik_info_fifth_question_expandable,cosmo_gorbik_info_sixth_question_expandable;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        cosmoGorbikInfoFragQuestionView=inflater.inflate(R.layout.fragment_cosmo_gorbik_info_frag_question, container, false);

        cosmo_gorbik_info_first_question_expandable =cosmoGorbikInfoFragQuestionView.findViewById(R.id.cosmo_gorbik_info_first_question_expandable);
        cosmo_gorbik_info_second_question_expandable =cosmoGorbikInfoFragQuestionView.findViewById(R.id.cosmo_gorbik_info_second_question_expandable);
        cosmo_gorbik_info_third_question_expandable =cosmoGorbikInfoFragQuestionView.findViewById(R.id.cosmo_gorbik_info_third_question_expandable);
        cosmo_gorbik_info_fourth_question_expandable =cosmoGorbikInfoFragQuestionView.findViewById(R.id.cosmo_gorbik_info_fourth_question_expandable);
        cosmo_gorbik_info_fifth_question_expandable =cosmoGorbikInfoFragQuestionView.findViewById(R.id.cosmo_gorbik_info_fifth_question_expandable);
        cosmo_gorbik_info_sixth_question_expandable =cosmoGorbikInfoFragQuestionView.findViewById(R.id.cosmo_gorbik_info_sixth_question_expandable);


        cosmo_gorbik_info_first_question_expandable.parentLayout.setOnClickListener(view ->
        {
            if (cosmo_gorbik_info_first_question_expandable.isExpanded())
                cosmo_gorbik_info_first_question_expandable.collapse();
            else cosmo_gorbik_info_first_question_expandable.expand();
        });

        cosmo_gorbik_info_second_question_expandable.parentLayout.setOnClickListener(view ->
        {
            if (cosmo_gorbik_info_second_question_expandable.isExpanded())
                cosmo_gorbik_info_second_question_expandable.collapse();
            else cosmo_gorbik_info_second_question_expandable.expand();
        });

        cosmo_gorbik_info_third_question_expandable.parentLayout.setOnClickListener(view ->
        {
            if (cosmo_gorbik_info_third_question_expandable.isExpanded())
                cosmo_gorbik_info_third_question_expandable.collapse();
            else cosmo_gorbik_info_third_question_expandable.expand();
        });

        cosmo_gorbik_info_fourth_question_expandable.parentLayout.setOnClickListener(view ->
        {
            if (cosmo_gorbik_info_fourth_question_expandable.isExpanded())
                cosmo_gorbik_info_fourth_question_expandable.collapse();
            else cosmo_gorbik_info_fourth_question_expandable.expand();
        });

        cosmo_gorbik_info_fifth_question_expandable.parentLayout.setOnClickListener(view ->
        {
            if (cosmo_gorbik_info_fifth_question_expandable.isExpanded())
                cosmo_gorbik_info_fifth_question_expandable.collapse();
            else cosmo_gorbik_info_fifth_question_expandable.expand();
        });

        cosmo_gorbik_info_sixth_question_expandable.parentLayout.setOnClickListener(view ->
        {
            if (cosmo_gorbik_info_sixth_question_expandable.isExpanded())
                cosmo_gorbik_info_sixth_question_expandable.collapse();
            else cosmo_gorbik_info_sixth_question_expandable.expand();
        });

        cosmoGorbikInfoSetAllText();

        return cosmoGorbikInfoFragQuestionView;
    }
    private void cosmoGorbikInfoSetAllText() {
        TextView cosmoGorbikInfoFirstParentText = cosmo_gorbik_info_first_question_expandable.
                parentLayout.findViewById(R.id.cosmo_gorbik_parent_question_text);
        cosmoGorbikInfoFirstParentText.setText(R.string.question_1);

        TextView cosmoGorbikInfoSecondParentText = cosmo_gorbik_info_second_question_expandable.
                parentLayout.findViewById(R.id.cosmo_gorbik_parent_question_text);
        cosmoGorbikInfoSecondParentText.setText(R.string.question_2);

        TextView cosmoGorbikInfoThirdParentText = cosmo_gorbik_info_third_question_expandable.
                parentLayout.findViewById(R.id.cosmo_gorbik_parent_question_text);
        cosmoGorbikInfoThirdParentText.setText(R.string.question_3);

        TextView cosmoGorbikInfoFourthParentText = cosmo_gorbik_info_fourth_question_expandable.
                parentLayout.findViewById(R.id.cosmo_gorbik_parent_question_text);
        cosmoGorbikInfoFourthParentText.setText(R.string.question_4);

        TextView cosmoGorbikInfoFifthParentText = cosmo_gorbik_info_fifth_question_expandable.
                parentLayout.findViewById(R.id.cosmo_gorbik_parent_question_text);
        cosmoGorbikInfoFifthParentText.setText(R.string.question_5);

        TextView cosmoGorbikInfoSixthParentText = cosmo_gorbik_info_sixth_question_expandable.
                parentLayout.findViewById(R.id.cosmo_gorbik_parent_question_text);
        cosmoGorbikInfoSixthParentText.setText(R.string.question_6);

        TextView cosmoGorbikInfoFirstChildText = cosmo_gorbik_info_first_question_expandable.secondLayout.
                findViewById(R.id.cosmo_gorbik_child_info_text);
        cosmoGorbikInfoFirstChildText.setText(R.string.answer_1);

        TextView cosmoGorbikInfoSecondChildText = cosmo_gorbik_info_second_question_expandable.secondLayout.
                findViewById(R.id.cosmo_gorbik_child_info_text);
        cosmoGorbikInfoSecondChildText.setText(R.string.answer_2);

        TextView cosmoGorbikInfoThirdChildText = cosmo_gorbik_info_third_question_expandable.secondLayout.
                findViewById(R.id.cosmo_gorbik_child_info_text);
        cosmoGorbikInfoThirdChildText.setText(R.string.answer_3);

        TextView cosmoGorbikInfoFourthChildText = cosmo_gorbik_info_fourth_question_expandable.secondLayout.
                findViewById(R.id.cosmo_gorbik_child_info_text);
        cosmoGorbikInfoFourthChildText.setText(R.string.answer_4);

        TextView cosmoGorbikInfoFifthChildText = cosmo_gorbik_info_fifth_question_expandable.secondLayout.
                findViewById(R.id.cosmo_gorbik_child_info_text);
        cosmoGorbikInfoFifthChildText.setText(R.string.answer_5);

        TextView cosmoGorbikInfoSixthChildText = cosmo_gorbik_info_sixth_question_expandable.secondLayout.
                findViewById(R.id.cosmo_gorbik_child_info_text);
        cosmoGorbikInfoSixthChildText.setText(R.string.answer_6);
    }
}